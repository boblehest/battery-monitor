{
  inputs.pyproject-nix = {
    url = "github:nix-community/pyproject.nix";
    inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, pyproject-nix, ... }:
    let
      inherit (nixpkgs) lib;
      project = pyproject-nix.lib.project.loadPyproject {
        projectRoot = ./.;
      };
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
      };

      thisPkg = self.packages.${system}.default;

      python = pkgs.python3.override {
        packageOverrides = self: super: {
          # NOTE This is to resolve package name mismatch between pyproject.toml and nixpkgs.
          pygobject = pkgs.python3Packages.pygobject3;
        };
      };
    in
    {
      homeManagerModules.default = ./module.nix;
      overlays.default = _: _: { jlo.battery-monitor = thisPkg; };

      devShells.x86_64-linux.default =
        let
          arg = project.renderers.withPackages { inherit python; };
          pythonEnv = python.withPackages arg;
        in
        pkgs.mkShell {
          packages = [ pythonEnv pkgs.pyright pkgs.ruff ];
        };
      packages.x86_64-linux.default =
        let
          both = lib.mergeAttrsWithFunc lib.concat;
          attrs = both (project.renderers.buildPythonPackage {
            inherit python;
          }) {
            nativeBuildInputs = [ pkgs.wrapGAppsHook ];
          };
        in
        python.pkgs.buildPythonApplication attrs;
    };
}
