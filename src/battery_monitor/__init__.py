import dbus
import notify2
from gi.repository import GLib
from dbus.mainloop.glib import DBusGMainLoop
import sys
DBusGMainLoop(set_as_default=True)

battery_warn_threshold = 0.25
notify2.init("BatteryAlarm")

def main():
    bus = dbus.SystemBus()
    upower = bus.get_object('org.freedesktop.UPower', '/org/freedesktop/UPower')
    devPaths = dbus.Interface(upower, 'org.freedesktop.UPower').EnumerateDevices()

    found = False
    for devPath in devPaths:
        dev = bus.get_object('org.freedesktop.UPower', devPath)
        properties = dbus.Interface(dev, 'org.freedesktop.DBus.Properties')
        devProps = properties.GetAll('org.freedesktop.UPower.Device')
        if 2 == devProps['Type']:
            if found:
                print("Found more than one battery -- aborting", file=sys.stderr)
                exit(1)
            found = True

            full_energy = devProps['EnergyFull']
            is_low = False
            def handler(a, dict, *args):
                nonlocal is_low
                try:
                    energy = dict['Energy']
                    energy_ratio = energy / full_energy
                    if (energy_ratio <= battery_warn_threshold) != is_low:
                        if not is_low:
                            notify2.Notification(
                                'Low battery',
                                f'The battery level is below {battery_warn_threshold * 100:.0f}%').show()
                        is_low = not is_low
                except KeyError:
                    pass

            properties.connect_to_signal('PropertiesChanged', handler)

    if not found:
        print("Found no batteries -- aborting", file=sys.stderr)
        exit(1)

    loop = GLib.MainLoop()
    loop.run()

