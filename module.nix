{ config, lib, pkgs, ... } : let
  cfg = config.services.jlo.battery-monitor;
in
  {
    options.services.jlo.battery-monitor = {
      enable = lib.mkEnableOption "Enables the battery-monitor service";
    };

    config = lib.mkIf cfg.enable {
      systemd.user.services."jlo.battery-monitor" = {
        Install.WantedBy = [ "graphical-session.target" ];
        Service.ExecStart = "${pkgs.jlo.battery-monitor}/bin/battery-monitor";
        Unit = {
          After = [ "graphical-session-pre.target" ];
          PartOf = [ "graphical-session.target" ];
        };
      };
    };
  }

